import logging
import base64
import json

from cerberus import Validator

from mg_errors import MegalodonParseError

log = logging.getLogger(__name__)


def validate(doc: dict, schema: dict):
    validator = Validator(schema)
    validator.allow_unknown = False

    if not validator.validate(doc):
        raise MegalodonParseError('Validation failed')

    return doc


MEGALODON_PAYLOAD_SCHEMA = {
    'v': {'type': 'number', 'required': True},
    'proofs': {'type': 'list'}
}

BASE_PROOF_SCHEMA = {
    'service': {'type': 'service'},
    'username': {'type': 'username'},
    'proof': {
        'type': 'dict',
        'allow_unknown': False,
        'shcema': {
            'url': {'type': 'string'},
        },
    }
}


class MegalodonData:
    """Main data class for megalodon payloads."""
    def __init__(self, raw_data: str):
        raw = base64.a85decode(raw_data.encode())
        self._raw = raw
        self._update()

    def _update(self):
        try:
            self._raw_json = json.loads(self._raw.decode())
        except (json.JSONDecodeError, UnicodeDecodeError):
            raise MegalodonParseError('Invalid JSON')

        # validate base
        payload = validate(self._raw_json, MEGALODON_PAYLOAD_SCHEMA)

        # validate all proofs
        proofs = []
        for idx, proof in payload['proofs']:
            proofs[idx] = validate(proof, BASE_PROOF_SCHEMA)

        self._payload = payload
        self.version = payload['v']
        self.proofs = proofs
        log.info(f'Megalodon payload v{self.version}')

    def __repr__(self):
        return (f'<MegalodonData v={self.version} '
                f'proof_count={len(self.proofs)}>')

    def to_json(self):
        pass
