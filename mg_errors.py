class MegalodonError(Exception):
    @property
    def message(self):
        return self.args[0]


class MegalodonParseError(MegalodonError):
    pass
