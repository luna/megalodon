from setuptools import setup

setup(
    name='megalodon',
    version='0.1',
    py_modules=['megalodon'],
    install_requires=[
        'Click==6.7',
        'requests==2.19.1',
        'ruamel.yaml==0.15.60',
        'Mastodon.py==1.3.1',
        'beautifulsoup4==4.6.3',
        'cerberus==1.2'
    ],
    entry_points='''
    [console_scripts]
    megalodon=megalodon:cli
    '''
)
