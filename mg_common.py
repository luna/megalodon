from bs4 import BeautifulSoup


def extract(status_content: str) -> str:
    content = BeautifulSoup(status_content, 'lxml')
    return content.p.string


def fetch_megalodon_raw(mctx, account: dict) -> str:
    """Fetch the raw megalodon toot."""
    statuses = mctx.mastodon.account_statuses(
        account.id,
        pinned=True,
        exclude_replies=True,

        # max megalodon payload is (AROUND) 5k chars, which
        # should be enough
        limit=10
    )

    try:
        # try to find the root megalodon toot
        root_mg = [status for status in statuses
                   if status.spoiler_text == 'MEGALODON ROOT'][0]
    except IndexError:
        return None

    # search any replies to the root as they're extensions
    # (to overcome 500 chars)
    tootctx = mctx.mastodon.status_context(root_mg.id)
    replies = tootctx['descendants']

    extensions = [
        status for status in replies
        if status.account.id == account.id and
        status.spoiler_text == 'MEGALODON EXT'
    ]

    # construct main payload
    mg_payload = ''.join(
        [extract(root_mg.content)] + [extract(s.content) for s in extensions])

    return mg_payload


def parse_mg_data(mg_data: str):
    pass
