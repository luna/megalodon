import sys
import json
import logging
import urllib.parse
from pathlib import Path

import click
import requests
from ruamel.yaml import YAML
from mastodon import Mastodon

from mg_common import fetch_megalodon_raw
from mg_errors import MegalodonError, MegalodonParseError
from mg_main import MegalodonData

logging.basicConfig(level=logging.DEBUG)

CONF_PATH = Path.home() / '.megalodon.yml'
DEFAULT_CONF = {
    'instance': None,
    'account': {},
    'proofs': []
}


def err(msg):
    click.secho(msg, fg='red')


def base(instance_url: str, path: str = '') -> str:
    return f'https://{instance_url}{path}'


def construct_url(base: str, params: dict) -> str:
    query_params = urllib.parse.urlencode(params)
    return f'{base}?{query_params}'


def save_cfg(ctx):
    ctx.conf.dump(ctx.cfg, CONF_PATH)


def require_correct(ctx):
    if not ctx.mastodon or not getattr(ctx, 'mg', False):
        click.secho('Megalodon configuration not found.', fg='red')
        click.secho('Please run the "setup" subcommand.', fg='red')
        sys.exit(0)


@click.group()
@click.pass_context
def cli(ctx):
    conf = YAML(typ='safe')
    ctx.conf = conf

    try:
        cfg = conf.load(CONF_PATH)
    except FileNotFoundError:
        conf.default_flow_style = False
        conf.dump(DEFAULT_CONF, CONF_PATH)
        cfg = DEFAULT_CONF

    if cfg['instance']:
        ctx.mastodon = Mastodon(
            access_token=cfg['access_token'],
            api_base_url=f'https://{cfg["instance"]}'
        )

        ctx.myself = ctx.mastodon.account_verify_credentials()
        my_mg = fetch_megalodon_raw(ctx, ctx.myself)

        if my_mg:
            try:
                ctx.mg = MegalodonData(my_mg)
            except MegalodonError as errobj:
                err('An error has occoured while reading '
                    f'your megalodon payload: {errobj.message}')

                sys.exit(0)
    else:
        ctx.mastodon = None
        ctx.myself = None
        ctx.mg = None

    ctx.cfg = cfg


@cli.command()
@click.pass_context
def setup(ctx):
    if ctx.parent.mastodon:
        click.secho('A Megalodon configuration already exists.', fg='yellow')

    click.secho('Welcome to megalodon!')
    click.secho('This is the account setup wizard.')
    click.secho('Please input the URL of your instance.')

    instance_url = input('URL: ')

    # create the app
    resp = requests.post(f'https://{instance_url}/api/v1/apps', params={
        'client_name': 'Megalodon',
        'redirect_uris': 'urn:ietf:wg:oauth:2.0:oob',
        'scopes': 'read write'
    })

    try:
        respjson = resp.json()
    except json.JSONDecodeError:
        return err('Failed to decode json from app creation')

    client_id, client_secret = respjson['client_id'], respjson['client_secret']

    auth_url = construct_url(base(instance_url, '/oauth/authorize'), {
        'scope': 'read write',
        'response_type': 'code',
        'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
        'client_id': client_id,
    })

    click.secho('Visit this URL in your browser:')
    click.secho(auth_url)
    click.secho('Then enter the resulting code here:')
    auth_code = input('Auth Code: ').strip()

    token_resp = requests.post(
        base(instance_url, '/oauth/token'),
        params={
            'client_id': client_id,
            'client_secret': client_secret,
            'grant_type': 'authorization_code',
            'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
            'code': auth_code
        }
    )

    try:
        token_json = token_resp.json()
    except json.JSONDecodeError:
        return err('Failed to decode JSON for final token request')

    cfg = ctx.parent.cfg

    # write what we got
    cfg['instance'] = instance_url
    cfg['client_id'] = client_id
    cfg['client_secret'] = client_secret
    cfg['access_token'] = token_json['access_token']

    save_cfg(ctx.parent)

    # query account details
    mastodon = Mastodon(
        access_token=cfg['access_token'],
        api_base_url=f'https://{instance_url}',
    )

    account = mastodon.account_verify_credentials()

    cfg['account'] = {
        'id': account['id'],
        'username': account['username'],
    }

    save_cfg(ctx.parent)

    fediurl = f'{account["username"]}@{instance_url}'
    click.secho(f'Setup done for {fediurl}', fg='green')


@cli.command()
@click.pass_context
def self(ctx):
    mctx = ctx.parent
    require_correct(mctx)

    display = mctx.myself.display_name or mctx.myself.username

    click.secho('Account details:', fg='green')
    click.secho(f'\tInstance: {mctx.cfg["instance"]}')
    click.secho(f'\tUsername: {display}')

    click.secho(f'\tMegalodon payload version: {mctx.mg.version}')
    click.secho(f'\tProofs: {len(mctx.mg.proofs)}')


@cli.command()
@click.argument('user_acct')
@click.pass_context
def verify(ctx, user_acct: str):
    mctx = ctx.parent

    search_results = mctx.mastodon.search(user_acct)
    accts = search_results['accounts']

    if not accts:
        return err('No users found')

    if len(accts) > 1:
        click.secho(f'{len(accts)} accounts found, which one would you want?')

        for idx, acct in enumerate(accts):
            display = f'{acct["display_name"]} - {acct["acct"]}'
            click.secho(f' - {idx}: {display}')

        choice = int(input('Choice: '))

        try:
            acct = accts[choice]
        except IndexError:
            return err('Invalid choice')
    else:
        acct = accts[0]

    click.secho(f'Username: {acct.username}', fg='green')
    click.secho(f'Display Name: {acct.display_name}', fg='green')
    click.secho(f'Account: {acct.acct}', fg='green')

    mg_data = fetch_megalodon_raw(mctx, acct)

    if not mg_data:
        return err('No Megalodon payload found.')

    try:
        mg = MegalodonData(mg_data)
    except (MegalodonError, MegalodonParseError):
        return err('User contains invalid Megalodon payload')

    click.secho(f'\tMegalodon payload version: {mg.version}')
    click.secho(f'\tProofs: {len(mg.proofs)}')


@cli.group()
@click.pass_context
def proof(ctx):
    pass


@proof.group(name='add')
@click.pass_context
def proof_add(ctx):
    pass


@proof_add.command(name='github')
@click.pass_context
def add_github(ctx):
    pass
