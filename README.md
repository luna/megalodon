# megalodon

megalodon is a system to provide keybase-like proofs for Mastodon profiles.

It works by using one of your profile fields to encode proof information
that other people can check.

## Installation

Requirements:
 - Python 3.6+

```bash
git clone https://gitlab.com/lnmds/megalodon

cd megalodon

pip3 install --user --editable .
```

## Security

Keep in mind that while megalodon gives keybase-like proof systems,
**it is not Keybase.** megalodon does not do cryptographic operations.

A malicious instance admin can fabricate a perfectly valid megalodon
payload and put it in your profile.

## Usage

**Please read the Security section before using this software.**

To start setting things up, run this in your shell:

```bash
megalodon setup
```

A configuration file on `~/.megalodon.yml` will be created. Do not
share that file with anyone else, as it contains credentials
to access your account.

After setting up megalodon you can:

 - check your information with `megalodon self`
 - **TODO: check a user's proofs with `megalodon view`**
 - verify other user's megalodon proofs with `megalodon verify`
 - **TODO: add proofs to your account via `megalodon proofs add <account_type>`**
